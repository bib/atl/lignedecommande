# Atelier ligne de commande dans un environment GNU/linux

Plan / résumé de l'atelier du 16/01/2018 - 20h~22h

## Comment consulter les sources des fichiers sur le framagit?


-> Explications sur GIT !!

  - GIT est un système de gestion de version 
  - un dépôt est l'ensemble des modifications
  - comment cloner un dépôt : 'git clone https://framagit.org/bib/ateliers/lignedecommande'
  - consulter l'historique des modifications : 'git log'
  - faire des modifications et les enregistrer 'git add' / 'git commit'
  - récupérer les modifications distantes : 'git pull'
